using Documenter, Installia

makedocs(;
    modules=[Installia],
    format=Documenter.HTML(),
    pages=[
        "Home" => "index.md",
    ],
    repo="https://gitlab.com/seamsay/Installia.jl/blob/{commit}{path}#L{line}",
    sitename="Installia.jl",
    authors="Sean Marshallsay",
    assets=String[],
)
