# Installia

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://seamsay.gitlab.io/Installia.jl/dev)
[![Build Status](https://gitlab.com/seamsay/Installia.jl/badges/master/build.svg)](https://gitlab.com/seamsay/Installia.jl/pipelines)
[![Coverage](https://gitlab.com/seamsay/Installia.jl/badges/master/coverage.svg)](https://gitlab.com/seamsay/Installia.jl/commits/master)
